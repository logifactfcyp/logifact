﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.Net.Attributes;

namespace LogiFact
{
    public class PRODUIT
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string LIBELLE { get; set; }
        public float PRIX { get; set; }
    }
}
