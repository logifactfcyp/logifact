﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.Net.Attributes;


namespace LogiFact
{
    public class CLIENT
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string NOM { get; set; }
        public string PRENOM { get; set; }
        public string NUMERO_TELEPHONE { get; set; }
        public string ADRESSE { get; set; }
    }
}
