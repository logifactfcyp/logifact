﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.Net.Attributes;

namespace LogiFact
{
    public class PRODUIT_FACTURE
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public int QUANTITE { get; set; }
        [ForeignKey("FACTURE")]
        public int FACTURE_ID { get; set; }
        [ForeignKey("PRODUIT")]
        public int PRODUIT_ID { get; set; }
    }
}
