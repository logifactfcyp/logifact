﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using SQLite.Net.Attributes;


namespace LogiFact
{
    public class FACTURE
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        [ForeignKey("CLIENT")]
        public int UTILISATEUR_ID { get; set; }
    }
}
