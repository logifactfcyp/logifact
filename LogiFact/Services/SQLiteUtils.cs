﻿using SQLite.Net;
using SQLite.Net.Platform.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LogiFact
{
    public sealed class SQLiteUtils
    {
        private static SQLiteUtils instance = null;
        private static SQLiteConnection connexion = null;
        private static SQLitePlatformWin32 _platform = new SQLitePlatformWin32();
        private static string data_source = "LogiFact.db";
        private static readonly object padlock = new object();

        SQLiteUtils()
        {
            connexion = GetSQLiteConnection();
        }

        public static SQLiteUtils Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new SQLiteUtils();
                    }
                    return instance;
                } 
            }
        }

        public SQLiteConnection GetSQLiteConnection()
        {
            if (connexion == null)
            {
                SQLiteConnection new_connexion = new SQLiteConnection(_platform, data_source);
                connexion = new_connexion;
                connexion.CreateTable<CLIENT>();
                connexion.CreateTable<PRODUIT>();
                connexion.CreateTable<FACTURE>();
                connexion.CreateTable<PRODUIT_FACTURE>();
            }
            return connexion;
        }
    }
}
