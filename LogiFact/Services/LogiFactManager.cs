﻿using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiFact
{
    public sealed class LogiFactManager
    {
        private static LogiFactManager instance = null;
        private static readonly object padlock = new object();

        LogiFactManager()
        {
        }

        public static LogiFactManager Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new LogiFactManager();
                    }
                    return instance;
                }
            }
        }

        public TableQuery<CLIENT> GetTableCLIENT()
        {
            return SQLiteUtils.Instance.GetSQLiteConnection().Table<CLIENT>();
        }

        public TableQuery<PRODUIT> GetTablePRODUIT()
        {
            return SQLiteUtils.Instance.GetSQLiteConnection().Table<PRODUIT>();
        }

        public TableQuery<FACTURE> GetTableFACTURE()
        {
            return SQLiteUtils.Instance.GetSQLiteConnection().Table<FACTURE>();
        }

        public TableQuery<PRODUIT_FACTURE> GetTablePRODUIT_FACTURE()
        {
            return SQLiteUtils.Instance.GetSQLiteConnection().Table<PRODUIT_FACTURE>();
        }
    }
}
