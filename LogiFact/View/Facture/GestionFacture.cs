﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace LogiFact
{
    public partial class GestionFacture : UserControl

    {
        public GestionFacture()
        {
            InitializeComponent();
            NomClient.ItemsSource = LogiFactManager.Instance.GetTableCLIENT().ToList();
        }
    }
}
