﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace LogiFact
{
    public partial class GestionClient : UserControl
    {
                
        public GestionClient()
        {
            InitializeComponent();
            setClientGlobalItemsSource();
        }

        private void AjouterClient(object sender, RoutedEventArgs e)
        {
            CLIENT client = GetClient();
            SQLiteUtils.Instance.GetSQLiteConnection().Insert(client);
            setClientGlobalItemsSource();
        }

        private void ModifClient(object sender, RoutedEventArgs e)
        {
            CLIENT c = ((FrameworkElement)sender).DataContext as CLIENT;
            ModifClient window = new ModifClient(c);
            window.ShowDialog();
            setClientGlobalItemsSource();
        }

        private void SupprClient(object sender, RoutedEventArgs e)
        {
            CLIENT c = ((FrameworkElement)sender).DataContext as CLIENT;
            SQLiteUtils.Instance.GetSQLiteConnection().Delete(c);
            setClientGlobalItemsSource();
        }

        private CLIENT GetClient()
        {
            return new CLIENT()
            {
                NOM = NomClient.Text,
                PRENOM = PrenomClient.Text,
                NUMERO_TELEPHONE = NumeroClient.Text,
                ADRESSE = AdresseClient.Text
            };
        }

        private void setClientGlobalItemsSource()
        {
            client_global.ItemsSource = null;
            client_global.ItemsSource = LogiFactManager.Instance.GetTableCLIENT().ToList();
        }
    }
}
