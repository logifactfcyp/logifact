﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LogiFact
{
    /// <summary>
    /// Logique d'interaction pour ModifClient.xaml
    /// </summary>
    public partial class ModifClient : Window
    {
        private CLIENT client;

        public ModifClient(CLIENT c)
        {
            InitializeComponent();
            NomClient.Text = c.NOM;
            PrenomClient.Text = c.PRENOM;
            AdresseClient.Text = c.ADRESSE;
            NumeroClient.Text = c.NUMERO_TELEPHONE;
            client = c;
        }

        private void ModifierClient(object sender, RoutedEventArgs e)
        {
            client.NOM = NomClient.Text;
            client.PRENOM = PrenomClient.Text;
            client.ADRESSE = AdresseClient.Text;
            client.NUMERO_TELEPHONE = NumeroClient.Text;
            SQLiteUtils.Instance.GetSQLiteConnection().Update(client);
            Close();
        }
    }
}
