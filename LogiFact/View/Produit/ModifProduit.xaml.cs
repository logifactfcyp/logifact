﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LogiFact
{
    /// <summary>
    /// Logique d'interaction pour ModifProduit.xaml
    /// </summary>
    public partial class ModifProduit : Window
    {
        private PRODUIT produit;

        public ModifProduit(PRODUIT p)
        {
            InitializeComponent();
            LibelleProduit.Text = p.LIBELLE;
            PrixProduit.Text = p.PRIX.ToString();
            produit = p;
        }

        private void ModifierProduit(object sender, RoutedEventArgs e)
        {
            produit.LIBELLE = LibelleProduit.Text;
            produit.PRIX = int.Parse(PrixProduit.Text);
            SQLiteUtils.Instance.GetSQLiteConnection().Update(produit);
            Close();
        }
    }
}
