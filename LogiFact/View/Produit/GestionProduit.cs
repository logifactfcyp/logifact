﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace LogiFact
{
    public partial class GestionProduit : UserControl
    {
        public GestionProduit()
        {
            InitializeComponent();
            setProduitGlobalItemsSource();
        }

        private void AjouterProduit(object sender, RoutedEventArgs e)
        {
            PRODUIT produit = GetProduit();
            SQLiteUtils.Instance.GetSQLiteConnection().Insert(produit);
            setProduitGlobalItemsSource();
        }

        private void ModifProduit(object sender, RoutedEventArgs e)
        {
            PRODUIT p = ((FrameworkElement)sender).DataContext as PRODUIT;
            ModifProduit window = new ModifProduit(p);
            window.ShowDialog();
            setProduitGlobalItemsSource();
        }

        private void SupprProduit(object sender, RoutedEventArgs e)
        {
            PRODUIT p = ((FrameworkElement)sender).DataContext as PRODUIT;
            SQLiteUtils.Instance.GetSQLiteConnection().Delete(p);
            setProduitGlobalItemsSource();
        }

        private PRODUIT GetProduit()
        {
            return new PRODUIT()
            {
                LIBELLE = LibelleProduit.Text,
                PRIX = int.Parse(PrixProduit.Text),
            };
        }

        private void setProduitGlobalItemsSource()
        {
            produit_global.ItemsSource = null;
            produit_global.ItemsSource = LogiFactManager.Instance.GetTablePRODUIT().ToList();
        }
    }
}
